import logging
from operator import truediv

import azure.functions as func
from azure.storage.blob import BlobClient, BlobServiceClient
from azure.eventgrid import EventGridPublisherClient, EventGridEvent
from azure.core.credentials import AzureKeyCredential

import tempfile
import os
import uuid

def main(req: func.HttpRequest) -> func.HttpResponse:

    logging.info('Python HTTP trigger function processed a request.')
    
    requestBody = req.get_json()
    data = requestBody['Data']
    uuidForMessage = uuid.uuid4()
    blobConnectionString = os.environ["blobConnectionString"]
    containerName = requestBody['containerName']
    azureBlobReponse = sendMessagesToBlobStorage(data, uuidForMessage, blobConnectionString, containerName)

    if azureBlobReponse:
        azureEventGridResponse = postEventToEventGrid(uuidForMessage, containerName)
    else:
       return func.HttpResponse(f"Hello. This HTTP triggered function executed unsuccessfully.",
             status_code=500)
    
    if azureEventGridResponse:
        return func.HttpResponse(f"Hello. This HTTP triggered function executed successfully.",
            status_code=200)
    else:
        return func.HttpResponse(f"Hello. This HTTP triggered function executed unsuccessfully.",
            status_code=500)

def sendMessagesToBlobStorage(data, uuid, blobConnectionString, containerName):
    try:
        logging.info('Started processing of file to BLOB storage')
        # Write messages to tmp file
        temporaryFile = tempfile.TemporaryFile()

        # Format message and transform to bytes
        temporaryFile.write("{}".format(data).encode('utf-8'))
        temporaryFile.seek(0)
       
        blob_client = BlobClient.from_connection_string(conn_str=blobConnectionString, container_name=containerName, blob_name=str(uuid) + '.json')
        blob_client.upload_blob(temporaryFile)
        blob_client.close()

        # Close tmp file
        temporaryFile.close()
        return True
        
    except Exception as ex:
        print('Upload of ' + ' blob' + ' unsuccesfull')
        print(ex)
        return False

def postEventToEventGrid(uuid, containerName):
    try:
        key = os.environ["EG_ACCESS_KEY"]
        endpoint = os.environ["EG_TOPIC_HOSTNAME"]

        event = EventGridEvent(
            data={"name": "Bart",
                "container": containerName,
                "BlobName": str(uuid) + ".json"},
            subject="Data",
            event_type="Demo",
            data_version="2.0"
        )

        credential = AzureKeyCredential(key)
        client = EventGridPublisherClient(endpoint, credential)

        client.send(event)
        return True
        
    except Exception as ex:
        print('Upload of ' + ' blob' + ' unsuccesfull')
        print(ex)
        return False